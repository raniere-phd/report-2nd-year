#!/bin/bash
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/report-2nd-year"
docker compose build quarto --pull --no-cache --progress plain
docker image tag report-2nd-year_quarto $DOCKER_IMAGE
docker push -q $DOCKER_IMAGE
